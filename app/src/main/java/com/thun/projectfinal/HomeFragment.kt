package com.thun.projectfinal

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.coroutineScope
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.thun.projectfinal.adapter.SubjectAdapter
import com.thun.projectfinal.databinding.FragmentHomeBinding
import com.thun.projectfinal.viewmodel.SubjectViewModel
import com.thun.projectfinal.viewmodel.SubjectViewModelFactory
import kotlinx.coroutines.launch

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [HomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HomeFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    private lateinit var recyclerView: RecyclerView

    lateinit var id : Array<Int>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,

        ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return _binding?.root
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment HomeFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            HomeFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        recyclerView = binding.recyclerView
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        val subjectAdapter = SubjectAdapter{
            val action =
                HomeFragmentDirections.actionHomeFragmentToListFragment(position = it.id)
            view.findNavController().navigate(action)
        }
        recyclerView.adapter = subjectAdapter
        lifecycle.coroutineScope.launch {
            viewModel.getAll().collect {
                subjectAdapter.submitList(it)
            }
        }
        lifecycle.coroutineScope.launch{
            viewModel.getSumTypePlus().collect{
                binding.money.text = it.toString()
            }
        }
        binding.receiptBtn.setOnClickListener {
            val action = HomeFragmentDirections.actionHomeFragmentToReceiptFragment()
            view.findNavController().navigate(action)
        }

        binding.expensesBtn.setOnClickListener {
            val action = HomeFragmentDirections.actionHomeFragmentToExpensesFragment()
            view.findNavController().navigate(action)
        }
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    private val viewModel: SubjectViewModel by activityViewModels {
        SubjectViewModelFactory(
            (activity?.application as SubjectScheduleApplication).database.scheduleDao()
        )
    }
//    private fun bind(subject: Flow<Double>) {
//        binding.apply {
//            money.text = subject.getFormattedPrice()
//        }
//    }

}
