package com.thun.projectfinal

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.navArgs
import com.thun.projectfinal.databinding.FragmentReceiptBinding
import com.thun.projectfinal.model.Subject
import com.thun.projectfinal.viewmodel.SubjectViewModel
import com.thun.projectfinal.viewmodel.SubjectViewModelFactory

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ReceiptFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ReceiptFragment : Fragment() {
    private var _binding: FragmentReceiptBinding? = null
    private val binding get() = _binding!!
    private val navigationArgs: ListFragmentArgs by navArgs()
    private val viewModel: SubjectViewModel by activityViewModels {
        SubjectViewModelFactory(
            (activity?.application as SubjectScheduleApplication).database
                .scheduleDao()
        )
    }
    lateinit var subject: Subject

//    private fun isEntryValid(): Boolean {
//        return viewModel.isEntryValid(
//            binding.storyEdit.text.toString(),
//            binding.moneyEdit.text.toString(),
//        )
//    }

//    private fun addNewItem() {
//        if (isEntryValid()) {
//            viewModel.addNewSubject(
//                binding.storyEdit.text.toString(),
//                binding.moneyEdit.text.toString(),
//                type = "+"
//            )
//            val action = ReceiptFragmentDirections.actionReceiptFragmentToHomeFragment()
//            findNavController().navigate(action)
//        }
//    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_receipt, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        val id = navigationArgs.position
////        if (id > 0) {
////            viewModel.retrieveItem(id).observe(this.viewLifecycleOwner) { selectedItem ->
////                subject = selectedItem
////                bind(subject)
////            }
////        } else {
//            binding.okBtn.setOnClickListener {
//                addNewItem()
////            }
//        }
    }

//    private fun bind(subject: Subject) {
//        val price = "%.2f".format(subject.money)
//        binding.apply {
//            storyEdit.setText(subject.story, TextView.BufferType.SPANNABLE)
//            moneyEdit.setText(price, TextView.BufferType.SPANNABLE)
//            okBtn.setOnClickListener { updateItem() }
//        }
//    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ReceiptFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ReceiptFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}