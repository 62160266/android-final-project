package com.thun.projectfinal

import android.app.Application
import com.thun.projectfinal.database.AppDatabase

class SubjectScheduleApplication: Application() {
    val database: AppDatabase by lazy {
        AppDatabase.getDatabase(this)
    }
}