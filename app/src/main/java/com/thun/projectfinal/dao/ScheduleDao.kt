package com.thun.projectfinal.dao

import androidx.room.*
import com.thun.projectfinal.model.Subject
import kotlinx.coroutines.flow.Flow

@Dao
interface ScheduleDao {
    @Query("SELECT * FROM subject")
    fun getAll(): Flow<List<Subject>>

//    @Query("SELECT SUM(money) FROM subject WHERE type = '-'")
//    fun getSumTypeMinus(): Flow<Subject>

//    @Query("SELECT * FROM subject WHERE id = :id")
//    fun getItem(id: Int): Flow<Subject>

    @Query("SELECT Sum(money) FROM subject WHERE type = '+'")
    fun getSumTypePlus(): Flow<Double>

//    @Insert
//    suspend fun insert(subject: Subject)
//
//    @Update
//    suspend fun update(subject: Subject)
//
//    @Delete
//    suspend fun delete(subject: Subject)
}