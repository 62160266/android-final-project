package com.thun.projectfinal.viewmodel

import androidx.lifecycle.*
import com.thun.projectfinal.dao.ScheduleDao
import com.thun.projectfinal.model.Subject
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch


class SubjectViewModel(private val scheduleDao: ScheduleDao): ViewModel() {
    fun getAll(): Flow<List<Subject>> = scheduleDao.getAll()
    fun getSumTypePlus():Flow<Double> = scheduleDao.getSumTypePlus()
//    fun getSum(): Flow<Subject> {
//        return scheduleDao.getSumTypePlus()
//    }
//    val typePlus : Flow<Double> = scheduleDao.getSumTypePlus()

}
class SubjectViewModelFactory(private val scheduleDao: ScheduleDao):ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(SubjectViewModel::class.java)){
            @Suppress("UNCHECKED_CAST")
            return SubjectViewModel(scheduleDao) as T
        }
        throw IllegalAccessException("Unknown ViewModel class")
    }
}