package com.thun.projectfinal.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.thun.projectfinal.dao.ScheduleDao
import com.thun.projectfinal.model.Subject

@Database(entities = [Subject::class], version = 1, exportSchema = false)
abstract class AppDatabase: RoomDatabase() {
    abstract fun scheduleDao(): ScheduleDao
    companion object{
        @Volatile
        private var INSTANCE: AppDatabase? = null
        fun getDatabase(context: Context): AppDatabase{
            return INSTANCE?: synchronized(this){
                val instance = Room.databaseBuilder(context,
                    AppDatabase::class.java,
                    "app_database"
                ).createFromAsset("database/subject.db")
                    .build()
                INSTANCE = instance
                return instance
            }
        }
    }
}