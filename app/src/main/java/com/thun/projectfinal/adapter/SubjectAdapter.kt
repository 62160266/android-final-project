package com.thun.projectfinal.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.thun.projectfinal.databinding.ListItemBinding
import com.thun.projectfinal.model.Subject
import java.text.NumberFormat

class SubjectAdapter(private val onItemClicked: (Subject)->Unit) :ListAdapter<Subject, SubjectAdapter.SubjectViewHolder>(DiffCallback) {
    companion object {
        private val DiffCallback = object : DiffUtil.ItemCallback<Subject>() {
            override fun areItemsTheSame(oldItem: Subject, newItem: Subject): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Subject, newItem: Subject): Boolean {
                return oldItem == newItem
            }
        }
    }
    class SubjectViewHolder(private var binding: ListItemBinding): RecyclerView.ViewHolder(binding.root){
        @SuppressLint("SimpleFormat")
        fun bind(subject: Subject){
            binding.story.text = subject.story
            binding.amount.text = subject.type + subject.money
        }
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SubjectViewHolder {
        val viewHolder = SubjectViewHolder(
            ListItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
        viewHolder.itemView.setOnClickListener{
            val position = viewHolder.adapterPosition
            onItemClicked(getItem(position))
        }
        return viewHolder
    }

    override fun onBindViewHolder(holder: SubjectViewHolder, position: Int) {
        holder.bind(getItem(position))
    }


}